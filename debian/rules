#!/usr/bin/make -f
# -*- makefile -*-
#export DH_VERBOSE=1
export DESTDIR=$(CURDIR)/debian/tmp

include /usr/share/dpkg/default.mk

DEB_HOST_ARCH_BITS ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_BITS)
export DEB_BUILD_ARCH      ?= $(shell dpkg-architecture -qDEB_BUILD_ARCH)
export DEB_HOST_ARCH      ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

# support ccache for faster build
# cmake uses /usr/bin/c*
ifeq (yes,$(findstring yes,$(shell test -L /usr/lib/ccache/c++ && test -L /usr/lib/ccache/cc && echo -n yes)))
  extraopts += -DWITH_CCACHE=ON
endif

# try to save even more memory on some architectures
# see #849657 for hints.
# Reduce size of debug symbols to fix FTBFS due to the
# 2GB/3GB address space limits on 32bit
ifeq (32,$(DEB_HOST_ARCH_BITS))
export DEB_CFLAGS_MAINT_APPEND = -g1 -Os
export DEB_CXXFLAGS_MAINT_APPEND = -g1 -Os
endif

# we don't have NEON on armel.
ifeq ($(DEB_HOST_ARCH),armel)
    extraopts += -DHAVE_ARM_NEON=0
endif

# disable ceph-dencoder on 32bit except i386 to avoid g++ oom
ifneq (,$(filter $(DEB_HOST_ARCH), armel armhf hppa m68k mips mipsel powerpc sh4 x32))
    extraopts += -DDISABLE_DENCODER=1
endif

ifeq ($(shell dpkg-vendor --is Ubuntu && echo yes) $(DEB_HOST_ARCH), yes i386)
   skip_packages = -Nceph -Nceph-base -Nceph-mds -Nceph-mgr -Nceph-mon -Nceph-osd
endif

# minimise needless linking and link to libatomic
# The last is needed because long long atomic operations are not directly
# supported by all processor architectures
# The -Wl,--no-keep-memory should save memory but then will go slower. No choice,
# as mipsel FTBFS.
ifeq (32,$(DEB_HOST_ARCH_BITS))
export DEB_LDFLAGS_MAINT_APPEND= -Wl,--as-needed -latomic -Wl,--no-keep-memory
endif

ifneq (,$(filter $(DEB_HOST_ARCH), riscv64))
export DEB_LDFLAGS_MAINT_APPEND= -Wl,--as-needed -latomic
endif

# This avoids:
# d: not enough GOT space for local GOT entries
# /usr/bin/ld: BFD (GNU Binutils for Debian) 2.42 internal error, aborting at ../../bfd/elfxx-mips.c:10719 in _bfd_mips_elf_relocate_section
# /usr/bin/ld: Please report this bug.
# collect2: error: ld returned 1 exit status
ifneq (,$(filter $(DEB_HOST_ARCH), mips64el))
export DEB_CFLAGS_MAINT_APPEND = -mxgot
export DEB_CXXFLAGS_MAINT_APPEND = -mxgot
endif

# Fix build with fmt 9
export DEB_CXXFLAGS_MAINT_APPEND += -DFMT_DEPRECATED_OSTREAM -fpermissive

# Enable hardening
export DEB_BUILD_MAINT_OPTIONS = hardening=+all optimize=-lto

export DESTDIR=$(CURDIR)/debian/tmp

export JAVA_HOME=/usr/lib/jvm/default-java
## Set JAVAC to prevent FTBFS due to incorrect use of 'gcj' if found (see "m4/ac_prog_javac.m4").
export JAVAC=javac

extraopts += -DWITH_JAEGER=OFF
# This makes it so that we don't need arrow. This removes
# some features, but arrow is downloading xsimd at build time,
# so we have no choice, unless we package arrow 9.0.0, which
# is in the TODO list.
extraopts += -DWITH_RADOSGW_SELECT_PARQUET=OFF
extraopts += -DWITH_SYSTEM_UTF8PROC=ON
extraopts += -DWITH_OCF=ON -DWITH_NSS=ON -DWITH_PYTHON3=ON -DWITH_DEBUG=ON -DWITH_LTTNG=OFF
extraopts += -DWITH_MGR_DASHBOARD_FRONTEND=OFF
extraopts += -DWITH_PYTHON2=OFF -DMGR_PYTHON_VERSION=3
extraopts += -DWITH_PYTHON3=3
extraopts += -DWITH_CEPHFS_JAVA=ON
extraopts += -DWITH_CEPHFS_SHELL=ON
extraopts += -DWITH_TESTS=ON
extraopts += -DWITH_SYSTEM_BOOST=ON
extraopts += -DWITH_SYSTEM_LIBURING=ON
extraopts += -DWITH_EMBEDDED=OFF
extraopts += -DCMAKE_INSTALL_LIBEXECDIR=/usr/lib/${DEB_HOST_MULTIARCH}
extraopts += -DWITH_SYSTEMD=ON -DCEPH_SYSTEMD_ENV_DIR=/etc/default
extraopts += -DCMAKE_INSTALL_SYSCONFDIR=/etc
extraopts += -DCMAKE_INSTALL_SYSTEMD_SERVICEDIR=/lib/systemd/system
extraopts += -DWITH_RADOSGW_KAFKA_ENDPOINT=OFF
extraopts += -DCMAKE_BUILD_TYPE=RelWithDebInfo
extraopts += -DWITH_GRAFANA=ON
extraopts += -DCMAKE_C_FLAGS_RELWITHDEBINFO="$(CFLAGS)"
extraopts += -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="$(CXXFLAGS)"
extraopts += -DWITH_SYSTEM_PMDK=ON

ifneq (,$(findstring $(DEB_HOST_ARCH),amd64 arm64 ppc64el riscv64))
extraopts += -DWITH_RBD_RWL=ON
extraopts += -DWITH_RBD_SSD_CACHE=ON
extraopts += -DWITH_BLUESTORE_PMEM=ON
extraopts += -DWITH_SPDK=ON
else
# Disable SPDK as it generates a build which is no compatible
# with older CPU's which are still supported by Ubuntu.
extraopts += -DWITH_SPDK=OFF
endif

# Enable crimson build on supported architectures
#ifneq (,$(findstring $(DEB_HOST_ARCH),amd64 arm64 ppc64el s390x))
#extraopts += -DWITH_SEASTAR=ON -DSeastar_CXX_FLAGS=-DSEASTAR_DEFAULT_ALLOCATOR
#endif
extraopts += -DWITH_SEASTAR=OFF

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  extraopts += -DBOOST_J=$(NUMJOBS)
endif

# Disable SPDK as it generates a build which is no compatible
# with older CPU's which are still supported by Ubuntu.
extraopts += -DWITH_SPDK=OFF

MAX_PARALLEL ?= $(shell ./debian/calc-max-parallel.sh)

%:
	dh $@ --buildsystem=cmake --with javahelper,python3 $(MAX_PARALLEL)

override_dh_auto_clean:
	dh_auto_clean
	rm -f src/common/options/global.yaml
	rm -rf src/boost/b2 src/boost/bin.v2 src/boost/stage
	rm -rf src/boost/tools/build/src/engine/b2
	rm -f src/boost/project-config.jam src/liburing/config-host.h src/liburing/config-host.mak src/liburing/config.log
	rm -f etc/sysctl/90-ceph-osd.conf src/boost/tools/build/src/engine/bjam
	rm -f src/liburing/src/include/liburing/compat.h src/liburing/src/liburing.a
	rm -f src/liburing/src/liburing.so.* src/liburing/src/queue.ol src/liburing/src/queue.os src/liburing/src/register.ol src/liburing/src/register.os
	rm -f src/liburing/src/setup.ol src/liburing/src/setup.os src/liburing/src/syscall.ol src/liburing/src/syscall.os
	rm -rf src/pybind/mgr/rook/rook_client/ceph

override_dh_auto_build:
	# Generate headers first, as it seems there's a race condition
	# that appears on slower build hosts where the build happens
	# before the headers are built.
	dh_auto_build --buildsystem=cmake -- legacy-option-headers
	# Start the actual build
	dh_auto_build --buildsystem=cmake 

override_dh_auto_configure:
	env | sort
	cp src/common/options/global.yaml.in src/common/options/global.yaml
	dh_auto_configure --buildsystem=cmake -- $(extraopts)
# after trying to patch the various places where HAVE_ARM_NEON is used
# the easiest way to get rid of it on armel seems to be to patch the cmake
# cache file.
ifeq ($(DEB_HOST_ARCH),armel)
	sed 's,^HAVE_ARM_NEON.*,HAVE_ARM_NEON:INTERNAL=0,' -i obj-arm-linux-gnueabi/CMakeCache.txt
endif

override_dh_auto_install:
	dh_auto_install --buildsystem=cmake --destdir=$(DESTDIR)
	if [ ! -f $(DESTDIR)/usr/bin/ceph-dencoder ]; then \
	    cp debian/workarounds/ceph-dencoder-oom $(DESTDIR)/usr/bin/ceph-dencoder ;\
	    chmod 755 $(DESTDIR)/usr/bin/ceph-dencoder ;\
	fi
	install -D -m 644 udev/50-rbd.rules $(DESTDIR)/lib/udev/rules.d/50-rbd.rules
	install -D -m 644 src/etc-rbdmap $(DESTDIR)/etc/ceph/rbdmap
	install -D -m 644 etc/sysctl/90-ceph-osd.conf $(DESTDIR)/etc/sysctl.d/30-ceph-osd.conf
	install -D -m 440 sudoers.d/ceph-smartctl $(DESTDIR)/etc/sudoers.d/ceph-smartctl
	install -D -m 755 src/tools/rbd_nbd/rbd-nbd_quiesce $(DESTDIR)/usr/libexec/rbd-nbd/rbd-nbd_quiesce

	install -m 644 -D monitoring/ceph-mixin/prometheus_alerts.yml $(DESTDIR)/etc/prometheus/ceph/ceph_default_alerts.yml

	# NOTE: ensure that any versioned erasure coding test code is dropped
	#       from the package install - package ships unversioned modules.
	rm -f $(CURDIR)/debian/tmp/usr/lib/*/ceph/erasure-code/libec_*.so.*
	find $(CURDIR)/debian/tmp/usr/lib/*/ceph/erasure-code -type l -delete || :
	# avoid running out of disk space
	rm -rf $(CURDIR)/obj-*-linux-gnu

# doc/changelog is a directory, which confuses dh_installchangelogs
override_dh_installchangelogs:
	dh_installchangelogs --exclude doc/changelog

override_dh_installlogrotate:
	cp src/logrotate.conf debian/ceph-common.logrotate
	dh_installlogrotate -pceph-common

override_dh_installinit:
	cp src/init-radosgw debian/radosgw.init
	# install the systemd stuff manually since we have funny service names
	install -d -m0755 debian/ceph-common/etc/default
	install -m0644 etc/default/ceph debian/ceph-common/etc/default/
	install -d -m0755 debian/ceph-common/usr/lib/tmpfiles.d
	install -m 0644 -D systemd/ceph.tmpfiles.d debian/ceph-common/usr/lib/tmpfiles.d/ceph.conf

	dh_installinit --no-start
	dh_installinit -pceph-common --name=rbdmap --no-start
	dh_installinit -pceph-base --name ceph --no-start
	# NOTE(jamespage): Install previous ceph-mon service from packaging for upgrades
	install -d -m0755 debian/ceph-mon/lib/systemd/system
	install -m0644 debian/lib-systemd/system/ceph-mon.service debian/ceph-mon/lib/systemd/system

	# These aren't installing properly, let's do this by hand

	# ceph-common's rbdmap.service
	install -d -m0755 debian/ceph-common/lib/systemd/system
	install -m0644 systemd/rbdmap.service.in debian/ceph-common/lib/systemd/system
	mv debian/ceph-common/lib/systemd/system/rbdmap.service.in debian/ceph-common/lib/systemd/system/rbdmap.service
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/ceph-common/lib/systemd/system/rbdmap.service

	# ceph-base's ceph-crash.service
	install -d -m0755 debian/ceph-base/lib/systemd/system
	install -m0644 systemd/ceph-crash.service.in debian/ceph-base/lib/systemd/system
	mv debian/ceph-base/lib/systemd/system/ceph-crash.service.in debian/ceph-base/lib/systemd/system/ceph-crash.service
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/ceph-base/lib/systemd/system/ceph-crash.service

	# ceph-fuse
	install -d -m0755 debian/ceph-fuse/lib/systemd/system
	install -m0644 systemd/ceph-fuse.target debian/ceph-fuse/lib/systemd/system
	install -m0644 systemd/ceph-fuse@.service.in debian/ceph-fuse/lib/systemd/system
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/ceph-fuse/lib/systemd/system/ceph-fuse@.service.in
	mv debian/ceph-fuse/lib/systemd/system/ceph-fuse@.service.in debian/ceph-fuse/lib/systemd/system/ceph-fuse@.service

	# ceph-immutable-object-cache
	install -d -m0755 debian/ceph-immutable-object-cache/lib/systemd/system
	install -m0644 systemd/ceph-immutable-object-cache@.service.in debian/ceph-immutable-object-cache/lib/systemd/system
	mv debian/ceph-immutable-object-cache/lib/systemd/system/ceph-immutable-object-cache@.service.in debian/ceph-immutable-object-cache/lib/systemd/system/ceph-immutable-object-cache@.service
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/ceph-immutable-object-cache/lib/systemd/system/ceph-immutable-object-cache@.service

	# ceph-mds
	install -d -m0755 debian/ceph-mds/lib/systemd/system
	install -m0644 systemd/ceph-mds@.service.in debian/ceph-mds/lib/systemd/system
	mv debian/ceph-mds/lib/systemd/system/ceph-mds@.service.in debian/ceph-mds/lib/systemd/system/ceph-mds@.service
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/ceph-mds/lib/systemd/system/ceph-mds@.service
	install -m0644 systemd/ceph-mds.target debian/ceph-mds/lib/systemd/system/

	# ceph-mgr
	install -d -m0755 debian/ceph-mgr/lib/systemd/system
	install -m0644 systemd/ceph-mgr@.service.in debian/ceph-mgr/lib/systemd/system
	mv debian/ceph-mgr/lib/systemd/system/ceph-mgr@.service.in debian/ceph-mgr/lib/systemd/system/ceph-mgr@.service
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/ceph-mgr/lib/systemd/system/ceph-mgr@.service
	install -m0644 systemd/ceph-mgr.target debian/ceph-mgr/lib/systemd/system/

	# ceph-mon
	install -d -m0755 debian/ceph-mon/lib/systemd/system
	install -m0644 systemd/ceph-mon@.service.in debian/ceph-mon/lib/systemd/system
	mv debian/ceph-mon/lib/systemd/system/ceph-mon@.service.in debian/ceph-mon/lib/systemd/system/ceph-mon@.service
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/ceph-mon/lib/systemd/system/ceph-mon@.service
	install -m0644 systemd/ceph-mon.target debian/ceph-mon/lib/systemd/system/

	# ceph-osd
	install -d -m0755 debian/ceph-osd/lib/systemd/system
	install -m0644 systemd/ceph-osd@.service.in debian/ceph-osd/lib/systemd/system
	mv debian/ceph-osd/lib/systemd/system/ceph-osd@.service.in debian/ceph-osd/lib/systemd/system/ceph-osd@.service
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/ceph-osd/lib/systemd/system/ceph-osd@.service
	sed -i 's#@CMAKE_INSTALL_FULL_LIBEXECDIR@#/usr/lib#' debian/ceph-osd/lib/systemd/system/ceph-osd@.service
	install -m0644 systemd/ceph-osd.target debian/ceph-osd/lib/systemd/system/

	# ceph-volume
	install -d -m0755 debian/ceph-volume/lib/systemd/system
	install -m0644 systemd/ceph-volume@.service debian/ceph-volume/lib/systemd/system

	# cephfs-mirror
	install -d -m0755 debian/cephfs-mirror/lib/systemd/system
	install -m0644 systemd/cephfs-mirror@.service.in debian/cephfs-mirror/lib/systemd/system
	mv debian/cephfs-mirror/lib/systemd/system/cephfs-mirror@.service.in debian/cephfs-mirror/lib/systemd/system/cephfs-mirror@.service
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/cephfs-mirror/lib/systemd/system/cephfs-mirror@.service
	install -m0644 systemd/cephfs-mirror.target debian/cephfs-mirror/lib/systemd/system/

	# radosgw
	install -d -m0755 debian/radosgw/lib/systemd/system
	install -m0644 systemd/ceph-radosgw@.service.in debian/radosgw/lib/systemd/system
	mv debian/radosgw/lib/systemd/system/ceph-radosgw@.service.in debian/radosgw/lib/systemd/system/ceph-radosgw@.service
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/radosgw/lib/systemd/system/ceph-radosgw@.service
	install -m0644 systemd/ceph-radosgw.target debian/radosgw/lib/systemd/system/

	# rbd-mirror
	install -d -m0755 debian/rbd-mirror/lib/systemd/system
	install -m0644 systemd/ceph-rbd-mirror@.service.in debian/rbd-mirror/lib/systemd/system
	mv debian/rbd-mirror/lib/systemd/system/ceph-rbd-mirror@.service.in debian/rbd-mirror/lib/systemd/system/ceph-rbd-mirror@.service
	sed -i 's#@SYSTEMD_ENV_FILE@#/etc/default/ceph#' debian/rbd-mirror/lib/systemd/system/ceph-rbd-mirror@.service
	install -m0644 systemd/ceph-rbd-mirror.target debian/rbd-mirror/lib/systemd/system/


override_dh_installsystemd:
	# Ensure Debian/Ubuntu specific systemd units are NOT automatically enabled and started
	# Enable systemd targets only
	# Start systemd targets only
	dh_installsystemd --no-stop-on-upgrade --no-restart-after-upgrade -Xceph-mon.service -Xceph-osd.service -X ceph-mds.service

override_dh_strip:
	dh_strip -pceph-mds --dbg-package=ceph-mds-dbg
	dh_strip -pceph-fuse --dbg-package=ceph-fuse-dbg
	dh_strip -pceph-mgr --dbg-package=ceph-mgr-dbg
	dh_strip -pceph-mon --dbg-package=ceph-mon-dbg
	dh_strip -pceph-osd --dbg-package=ceph-osd-dbg
	dh_strip -pceph-base --dbg-package=ceph-base-dbg
	dh_strip -pcephfs-mirror --dbg-package=cephfs-mirror-dbg
	dh_strip -prbd-fuse --dbg-package=rbd-fuse-dbg
	dh_strip -prbd-mirror --dbg-package=rbd-mirror-dbg
	dh_strip -pceph-immutable-object-cache --dbg-package=ceph-immutable-object-cache-dbg
	dh_strip -prbd-nbd --dbg-package=rbd-nbd-dbg
	dh_strip -pceph-common --dbg-package=ceph-common-dbg
	dh_strip -plibrados2 --dbg-package=librados2-dbg
	dh_strip -plibsqlite3-mod-ceph --dbg-package=libsqlite3-mod-ceph-dbg
	dh_strip -plibradosstriper1 --dbg-package=libradosstriper1-dbg
	dh_strip -plibrbd1 --dbg-package=librbd1-dbg
	dh_strip -plibcephfs2 --dbg-package=libcephfs2-dbg
	dh_strip -plibrgw2 --dbg-package=librgw2-dbg
	dh_strip -pradosgw --dbg-package=radosgw-dbg
	dh_strip -pceph-test --dbg-package=ceph-test-dbg

	# No -dbg packages for python3 bindings.
	dh_strip -ppython3-cephfs
	dh_strip -ppython3-rados
	dh_strip -ppython3-rbd
	dh_strip -ppython3-rgw

	dh_strip -plibcephfs-jni
	dh_strip -plibrados-dev

override_dh_makeshlibs:
	# exclude jni libraries in libcephfs-jni to avoid pointless ldconfig
	# calls in maintainer scripts; exclude private erasure-code plugins.
	dh_makeshlibs -V -X/usr/lib/jni -X/usr/lib/$(DEB_HOST_MULTIARCH)/ceph/erasure-code

override_dh_auto_test:
	# do not run tests

override_dh_shlibdeps:
	dh_shlibdeps -a --exclude=erasure-code --exclude=rados-classes --exclude=compressor --exclude=ceph_crypto

override_dh_python3:
	for binding in ceph ceph-argparse cephfs ceph-common rados rbd rgw; do \
		dh_python3 -p python3-$$binding --shebang=/usr/bin/python3;      \
        done
	dh_python3 -p ceph-common --shebang=/usr/bin/python3
	dh_python3 -p ceph-base --shebang=/usr/bin/python3
	dh_python3 -p ceph-osd --shebang=/usr/bin/python3
	dh_python3 -p ceph-mgr --shebang=/usr/bin/python3
	dh_python3 -p cephfs-shell --shebang=/usr/bin/python3
	dh_python3 -p cephfs-top --shebang=/usr/bin/python3
	dh_python3 -p cephadm --shebang=/usr/bin/python3
	dh_python3 -p ceph-volume --shebang=/usr/bin/python3

override_dh_builddeb:
	dh_builddeb ${skip_packages}

override_dh_gencontrol:
	dh_gencontrol ${skip_packages}

override_dh_fixperms:
	dh_fixperms
	# If the package builds on buildd, the file will not exists for arch:all,
	# so we must make this a conditional.
	if [ -e $(CURDIR)/debian/ceph-osd/usr/lib/ceph/ceph-osd-prestart.sh ] ; then \
		chmod +x $(CURDIR)/debian/ceph-osd/usr/lib/ceph/ceph-osd-prestart.sh ; \
	fi
	for i in 	$(CURDIR)/debian/ceph-base/usr/lib/ceph/ceph_common.sh \
			$(CURDIR)/debian/ceph-mgr-dashboard/usr/share/ceph/mgr/dashboard/ci/cephadm/bootstrap-cluster.sh \
			$(CURDIR)/debian/ceph-mgr-dashboard/usr/share/ceph/mgr/dashboard/ci/cephadm/run-cephadm-e2e-tests.sh \
			$(CURDIR)/debian/ceph-mgr-dashboard/usr/share/ceph/mgr/dashboard/ci/cephadm/start-cluster.sh \
			$(CURDIR)/debian/ceph-mgr-dashboard/usr/share/ceph/mgr/dashboard/run-backend-api-request.sh \
			$(CURDIR)/debian/ceph-mgr-dashboard/usr/share/ceph/mgr/dashboard/run-backend-api-tests.sh \
			$(CURDIR)/debian/ceph-mgr-dashboard/usr/share/ceph/mgr/dashboard/run-backend-rook-api-request.sh \
			$(CURDIR)/debian/ceph-mgr-dashboard/usr/share/ceph/mgr/dashboard/run-frontend-e2e-tests.sh \
			$(CURDIR)/debian/ceph-mgr-dashboard/usr/share/ceph/mgr/dashboard/run-frontend-unittests.sh \
			$(CURDIR)/debian/ceph-mgr-rook/usr/share/ceph/mgr/rook/ci/run-rook-e2e-tests.sh \
			$(CURDIR)/debian/ceph-mgr-rook/usr/share/ceph/mgr/rook/ci/scripts/bootstrap-rook-cluster.sh \
			$(CURDIR)/debian/ceph-mgr-rook/usr/share/ceph/mgr/rook/generate_rook_ceph_client.sh ; do \
		if [ -e $$i ] ; then \
			chmod +x $$i ; \
		fi ; \
	done

execute_after_dh_install:
	if command -v dh_movetousr > /dev/null 2>&1; then dh_movetousr; fi
